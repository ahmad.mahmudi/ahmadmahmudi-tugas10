// import React, {useState} from 'react';
// import {View, TouchableOpacity, StyleSheet} from 'react-native';
// import {
//   TextRegular,
//   TextBold,
//   TextMedium,
//   Header,
//   InputText,
// } from '../../component/global';
// import {Colors} from '../../styles';
// import ModalBottom from '../../component/modal/ModalBottom';
// import ModalCenter from '../../component/modal/ModalCenter';
// import {NumberFormater, validateEmail} from '../../utils/Helper';

// const Login = ({navigation, route}) => {
//   const [modalBottom, setModalBottom] = useState(false);
//   const [modalCenter, setModalCenter] = useState(false);

//   const onLogin = () => {
//     const contohEmail = 'codemaster@gmail.com';
//     if (!validateEmail(contohEmail)) {
//       // pesan Error
//     } else {
//       // Pesan Success
//     }
//   };

//   return (
//     <View style={{flex: 1, backgroundColor: '#fff'}}>
//       <TouchableOpacity
//         style={styles.btnShowModalBottom}
//         onPress={() => setModalBottom(true)}>
//         <TextBold text="Modal Bottom" size={16} color="#fff" />
//       </TouchableOpacity>
//       <TouchableOpacity
//         onPress={() => setModalCenter(true)}
//         style={[
//           styles.btnShowModalBottom,
//           {backgroundColor: Colors.DEEPORANGE},
//         ]}>
//         <TextBold text="Modal Center" size={16} color="#fff" />
//       </TouchableOpacity>
//       <View>
//         <TextRegular
//           text={`${NumberFormater(1000000, 'Rp ')}`}
//           size={16}
//           color={Colors.BLACK}
//           style={{
//             marginTop: 20,
//             marginLeft: 20,
//           }}
//         />
//       </View>
//       <ModalBottom
//         show={modalBottom}
//         onClose={() => setModalBottom(false)}
//         title="Modal Bottom">
//         <View>
//           <TextBold text="Text Bold" />
//           <TextRegular text="Text Regular" style={{marginVertical: 20}} />
//           <TextMedium text="Text Medium" />
//         </View>
//       </ModalBottom>
//       <ModalCenter
//         show={modalCenter}
//         onClose={() => setModalCenter(false)}
//         title="Modal Center">
//         <View>
//           <TextBold text="Text Bold" />
//           <TextRegular text="Text Reguler" style={{marginVertical: 20}} />
//           <TextMedium text="Text Medium" />
//         </View>
//       </ModalCenter>
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   btnShowModalBottom: {
//     width: '35%',
//     justifyContent: 'center',
//     alignItems: 'center',
//     paddingVertical: 10,
//     borderRadius: 6,
//     backgroundColor: Colors.PRIMARY,
//     alignSelf: 'center',
//     marginTop: 20,
//   },
// });

// export default Login;

import { StyleSheet, Text, View } from 'react-native'
import React, { useState } from 'react'
import LoginComponent from '../../component/section/Auth/LoginComponent'

const Login = ({navigation, route}) => {

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [showPassword, setHidePassword] = useState(true)

  const onLogin = () => {}

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <LoginComponent 
      navigation={navigation}
      email={email}
      setEmail={(text) => setEmail(text)}
      password={password}
      setPassword={(text) => setPassword(text)}
      showPassword={showPassword}
      setShowPassword={(value)=> setHidePassword(value)}
      onPressLogin={() => onLogin()}/>
    </View>
  )
}

export default Login

const styles = StyleSheet.create({})