export const NumberFormater = (nominal = 0, currency) => {
  let rupiah = '';
  const nominalRef = nominal.toString().split('').reverse().join('');
  for (let i = 0; i < nominalRef.length; i++) {
    if (i % 3 === 0) {
      rupiah += `${nominalRef.substr(i, 3)}.`;
    }
  }

  if (currency) {
    return (
      currency +
      rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('')
    );
  }
  return rupiah
    .split('', rupiah.length - 1)
    .reverse()
    .join('');
};

export const validateEmail = email => {
  const re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};
